# Zillow API Example App

### Overview

Hi Zillow! Here's my coding challenge result. I choose Ruby/Rails just because I'm really comfortable (and fast) with it. There are a few unit tests in the `spec` folder to check out.

### Demo

You can see the app hosted on Heroku [here]( https://zillow-api.herokuapp.com). The API key is configured at `ZILLOW_API_KEY` in the host's environment.
